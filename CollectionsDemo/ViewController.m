//
//  ViewController.m
//  CollectionsDemo
//
//  Created by James Cash on 18-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CircleLayout.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UICollectionViewLayout *defaultLayout;
@property (strong, nonatomic) UICollectionViewFlowLayout *bigLayout;
@property (strong, nonatomic) UICollectionViewFlowLayout *smallLayout;
@property (strong, nonatomic) CircleLayout *circle;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.defaultLayout = self.collectionView.collectionViewLayout;

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.bigLayout.estimatedItemSize = CGSizeMake(300, 300);
//    self.bigLayout.itemSize = CGSizeMake(300, 300);
    self.bigLayout.minimumInteritemSpacing = 25;
    self.bigLayout.minimumLineSpacing = 75;
    self.bigLayout.headerReferenceSize = CGSizeMake(0, 250);
    self.bigLayout.footerReferenceSize = CGSizeMake(0, 100);

    self.smallLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.smallLayout.itemSize = CGSizeMake(100, 100);
//    self.smallLayout.estimatedItemSize = CGSizeMake(100, 100);
    self.smallLayout.minimumInteritemSpacing = 10;
    self.smallLayout.minimumLineSpacing = 0;
    self.smallLayout.headerReferenceSize = CGSizeMake(0, 50);
    self.smallLayout.footerReferenceSize = CGSizeZero;

    self.circle = [[CircleLayout alloc] init];

    self.collectionView.collectionViewLayout = self.smallLayout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)toggleLayout:(UIButton*)sender {
    sender.enabled = NO;
    UICollectionViewLayout *next;
    UICollectionViewLayout *current = self.collectionView.collectionViewLayout;
    if (current == self.defaultLayout) {
        next = self.bigLayout;
    } else if (current == self.bigLayout) {
        next = self.smallLayout;
    } else if (current == self.smallLayout) {
        next = self.circle;
    } else {
        next = self.defaultLayout;
    }
    [current invalidateLayout];
    [self.collectionView setCollectionViewLayout:next animated:YES completion:^(BOOL finished) {
        sender.enabled = YES;
    }];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3 + section;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];

    UILabel *cellLabel = (UILabel*)[cell viewWithTag:1];
    cellLabel.text = [NSString stringWithFormat:@"%ld : %ld", indexPath.section, indexPath.item];

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"Header" forIndexPath:indexPath];

        UILabel *label = [header viewWithTag:1];
        label.text = [NSString stringWithFormat:@"Header %ld", indexPath.section];
        return header;
    }

    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"Footer" forIndexPath:indexPath];

        UILabel *label = [footer viewWithTag:1];
        label.text = [NSString stringWithFormat:@"Footer %ld", indexPath.section];
        return footer;
    }

    return nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionViewLayout == self.bigLayout) {
        return CGSizeMake(200 + indexPath.item * 25,
                          200 + indexPath.item * 25);
    } else if (collectionViewLayout == self.smallLayout) {
        return CGSizeMake(150 - indexPath.item * 15,
                          150 - indexPath.item * 15);
    } else {
        return CGSizeMake(250, 150);
    }
}

@end
