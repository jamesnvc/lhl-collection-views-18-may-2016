//
//  ViewController.h
//  CollectionsDemo
//
//  Created by James Cash on 18-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>


@end

